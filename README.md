## A simple script for generating a time logging markdown document, based on messages from commits

**Usage:** Copy `timesheet.sh` file in the root of your project.

A new `TIMESHEET.md` file will be generated on every launch, with the updated working time logged in all of the commit messages.

---

**Syntax for commit message:** `<ignored_text> :stopwatch: <value> <ignored text>`

`<value>` represents minutes

_(e.g. Fixes #1 `:stopwatch: 120` minutes)_
